# Minimal shop prototype in w98 style

## About

Everything you will se in this website was inspired in some ways by this website: https://joelgc.com/
Some code was copypasted. If something works - why bother to make it yourself 
Also w98 stylesheet [https://unpkg.com/98.css] was heavily modified (in index.css LULE) bcs vanilla version sucks big stinky ass
Was added/made/modified in 98.css: 
- Window icon support
- Functional botbar with icons and shit 
- Windows js functionality (hide/show/etc (How about to make them draggable?))  
- Other little improvements I already forgot about

## TO-DO

### GENERAL

- [ ] Make own version of 98.css with all changes i made in index.css

### FUNCTIONALITY

### Shop:

- [ ] Add type filter support
- [ ] 'Favourites' support
 
### Cart: (high priority)

- [ ] Add item quantity buttons
- [ ] Update cart total in botbar on item in cart change
- [ ] Remove item when clicked on X
- [ ] Cart list generation: generate list of items when clicking on "checkout!" in JSON format

<hr>

### UX (pobably will never be )

### Shop:

- [ ] Mobile support (never)
- [ ] Rating on card
- [ ] Rating in item preview

### Cart: 

- [ ] Botbar cart animation when adding items (blinking)

